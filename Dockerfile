FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build

WORKDIR /sln


#copy everything
COPY ./ .


RUN apt-get update && apt-get install -y libcurl4

#build projects and restore packages
RUN dotnet restore

RUN dotnet build -c Release --no-restore

RUN dotnet publish -c Release -o "/sln/dist" --no-restore

FROM mcr.microsoft.com/dotnet/aspnet:7.0
ARG source
WORKDIR /app
COPY --from=build /sln/dist .

ENTRYPOINT ["dotnet", "cicdtest.dll"]

